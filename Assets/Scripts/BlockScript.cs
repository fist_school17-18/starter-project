﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockScript : MonoBehaviour {
    int HP = 100;
    public bool canDestroy = true;
    [Range(0, 5)] public int id;

    public GameObject crackPrefab;

    public void GetDamage (int value)
    {
        if (!canDestroy) return;
        HP -= value;
        if (HP <= 0)
        {
            GameObject go = Instantiate<GameObject>(
                crackPrefab,
                transform.position,
                transform.rotation
                //transform.parent
                // cracks don't belong to landscape!
            );
            foreach(Transform child in go.transform)
            {
                child
                    .GetComponent<PickUpMaterial>()
                    .id = id;
            }
            Destroy(gameObject);
        }
    }
}
