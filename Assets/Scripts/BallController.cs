﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BallController : MonoBehaviour {

	public RectTransform GOText;
	public RectTransform WinText;
	public RectTransform ScoreText;

	bool isFinished = false;
	int score = 0;

	void OnCollisionEnter (Collision other) {
		if (other.gameObject.tag == "Terrain" && isFinished == false) {
			print ("Gameover");
			GOText.gameObject.SetActive (true);
		}
	}

	void OnTriggerEnter (Collider other) {
		if (other.gameObject.tag == "Finish") {
			print ("Win");
			WinText.gameObject.SetActive (true);
			isFinished = true;
		}

		if (other.gameObject.tag.Contains("Teapot")) {
			score += other.gameObject.GetComponent<TeapotScript> ().cost;
			ScoreText.GetComponent<Text> ().text = score.ToString ();
			other.gameObject.GetComponent<Animator> ().PlayInFixedTime ("TeapotDestroyAnimation");
			Destroy (other.gameObject, 0.5f);
		}
	}
}
