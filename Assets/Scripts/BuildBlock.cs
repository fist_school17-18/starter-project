﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuildBlock : MonoBehaviour {

    public Transform marker;
    public Transform landscape;

    public Transform[] blockPrefabs;

    public RectTransform[] blockUI;
    public RectTransform[] matsUI;

    int selectedBlock = 0;
    Camera cam;
    int BLOCK_SIZE = 1;

    void Start()
    {
        cam = GetComponentInChildren<Camera>();
        blockUI[0].GetComponent<Image>().color = new Color(0, 1, 0, 0.392f);

        for (int i = 0; i < GetComponent<BackpackScript>().GetMatsLenght(); i++)
        {
            matsUI[i].GetComponentInChildren<Text>().text =
                GetComponent<BackpackScript>().GetMaterialCount(i).ToString();
        }

        for (int i = 0; i < GetComponent<BackpackScript>().GetBlockLenth(); i++)
        {
            blockUI[i].GetComponentInChildren<Text>().text =
                GetComponent<BackpackScript>().GetBlockCount(i).ToString();
        }
    }

    void Update ()
    {
        Ray ray = new Ray(cam.transform.position, cam.transform.forward);
        RaycastHit hit = new RaycastHit();
        Physics.Raycast(ray, out hit, 5);
        Collider target = hit.collider;
        if (target != null && target.gameObject.GetComponent<BlockScript>() != null)
        {
            // draw marker
            marker.position = target.transform.position + BLOCK_SIZE* hit.normal;
            marker.GetComponent<Renderer>().enabled = true;
        }
        else
        {
            // erase marker
            marker.GetComponent<Renderer>().enabled = false;
        }

        if (Input.GetMouseButtonDown(1)&&
            marker.GetComponent<Renderer>().enabled &&
            GetComponent<BackpackScript>().GetBlockCount(selectedBlock) > 0)
        {
            Vector3 pos = marker.position;
            Instantiate(blockPrefabs[selectedBlock], pos, Quaternion.identity, landscape);

            //GetComponent<BackpackScript>().blocksCount[selectedBlock]--;
            GetComponent<BackpackScript>().ChangeBlocks(selectedBlock, -1);


            //blockUI[selectedBlock].GetComponentInChildren<Text>().text =
            //    GetComponent<BackpackScript>().blocksCount[selectedBlock].ToString();
        }

        if (Input.GetAxis("Mouse ScrollWheel") != 0)
        {
            blockUI[selectedBlock].GetComponent<Image>().color = new Color(1, 1, 1, 0.392f);
            selectedBlock += System.Math.Sign(Input.GetAxis("Mouse ScrollWheel"));
            selectedBlock = (5 + selectedBlock) % 5;
            blockUI[selectedBlock].GetComponent<Image>().color = new Color(0, 1, 0, 0.392f);
        }
    }
}
