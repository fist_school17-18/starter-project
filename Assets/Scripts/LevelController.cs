﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class LevelController : MonoBehaviour {
	void Update () {
		float h = -CrossPlatformInputManager.GetAxis ("Horizontal");
		float v = CrossPlatformInputManager.GetAxis ("Vertical");
		Quaternion rot = transform.localRotation;

		rot.x += v * 0.002f;
		if (Mathf.Abs(rot.x) >= 0.05f) {
			rot.x -= v * 0.002f;	
		}

		rot.z += h * 0.002f;
		if (Mathf.Abs(rot.z) >= 0.05f) {
			rot.z -= h * 0.002f;	
		}

		transform.localRotation = rot;
	}
}