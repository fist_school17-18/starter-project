﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class MenuController : MonoBehaviour {

    public GameObject MenuCanvas;
    public GameObject HUD;

    public Transform[] slotPrefabs;
    public RectTransform content;

    bool visible = false;
    BackpackScript bp;
    CraftScript cs;

    private void Awake()
    {
        bp = GetComponent<BackpackScript>();
        cs = content.GetComponentInParent<CraftScript>();
    }

    void Update ()
    {
		if (Input.GetKeyDown(KeyCode.I))
        {
            visible = !visible;
            Pause();
        }
	}

    void Pause()
    {
        HUD.SetActive(!visible);
        MenuCanvas.SetActive(visible);
        GetComponent<CharacterController>().enabled = !visible;
        GetComponent<FirstPersonController>().enabled = !visible;
        Time.timeScale = visible ? 0 : 1;

        UnityEngine.Cursor.visible = visible;
        UnityEngine.Cursor.lockState = 
            visible ? CursorLockMode.None : CursorLockMode.Locked;

        if (visible)
        {
            for (int i = 0; i < bp.GetMatsLenght(); i++)
            {
                int count = bp.GetMaterialCount(i);
                for (int l = 0; l < count; l++)
                {
                    // instamtiate
                    Transform slot = Instantiate<Transform>(slotPrefabs[i]);
                    slot.SetParent(content);
                    slot.localScale = Vector3.one;
                }
            }
            content.sizeDelta = new Vector2(
                270,
                53 * (content.childCount / 5 + 1) + 5
            );
        }
        else
        {
            foreach (Transform slot in content)
                Destroy(slot.gameObject);

            foreach (RectTransform slot in cs.craftSlots)
            {
                if (slot.childCount > 0)
                    Destroy(slot.GetChild(0).gameObject);
            }
        }
    }
}
