﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class DropHandler : MonoBehaviour, IDropHandler
{
    public void OnDrop(PointerEventData eventData)
    {
        DragHandler item = eventData
            .pointerDrag
            .GetComponent<DragHandler>();
        if (item != null && transform.childCount == 0)
        {
            // drop item, set parent for item;
            item.startParent = transform;
            print(transform.name);
        }
    }
}
