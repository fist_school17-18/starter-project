﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    Animator anim;
    public int health = 100;
    List<GameObject> damageArea = new List<GameObject>();

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<BackpackScript>() ||
            (other.gameObject.GetComponent<BlockScript>() &&
                    other.gameObject.GetComponent<BlockScript>().canDestroy
                )    
            )
        {
            if (!damageArea.Contains(other.gameObject))
                damageArea.Add(other.gameObject);
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.GetComponent<BlockScript>()
            || other.gameObject.GetComponent<BackpackScript>())
        {
            damageArea.Remove(other.gameObject);
        }
    }

    void Start ()
    {
        anim = GetComponent<Animator>();
    }
	
	void Update ()
    {

        if (anim.GetInteger("State") >= 3 && 
            anim.GetCurrentAnimatorStateInfo(0).normalizedTime < 1)
            return;

        if (damageArea.Count > 0)
        {
            anim.SetInteger("State", 2);
        }
        else
        {
            anim.SetInteger("State", 0);
        }
    }

    public void GetDamage(int dmg)
    {
        anim.SetInteger("State", 3);
        health -= dmg;

        if (health <= 0)
        {
            anim.SetInteger("State", 4);
            foreach (Collider collider in GetComponents<Collider>())
            {
                //collider.enabled = false;
                Destroy(collider);
            }
            //gameObject.GetComponent<Enemy>().enabled = false;
            this.enabled = false;
        }
    }

    public void Attack()
    {
        damageArea.RemoveAll(s => s == null); // удвление всех элементов списка, которые равны null
        foreach (GameObject item in damageArea)
        {
            if (item.GetComponent<BlockScript>())
                item.GetComponent<BlockScript>().GetDamage(40);
#warning прекращать атаку, ссли игрок мертв
            else
                item.GetComponent<BackpackScript>().GetDamage(40);

        }
    }
}
