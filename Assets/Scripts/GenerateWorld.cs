﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Threading;

public class GenerateWorld : MonoBehaviour {

    public int size;
    public Transform[] blocksPrefabs;

    string path;

    void Awake()
    {
        path = Application.dataPath + "/world.txt";
        if (File.Exists(path))
        {
            Load();
        }
        else
        {
            Create();
        }
    }

    void OnDestroy()
    {
        Save();
    }

    void Create()
    {
        int seed = Random.Range(0, 16);
        for (int x = 0; x < size; x++)
        {
            for (int z = 0; z < size; z++)
            {
                int y = (int)(8 * Mathf
                    .PerlinNoise(seed + x / 16.0f, seed + z / 16.0f));
                Vector3 pos = new Vector3(x, y, z);
                Instantiate(
                    blocksPrefabs[0],
                    pos,
                    Quaternion.identity,
                    this.transform
                );
            }
        }
    }

    void Save()
    {
        string[] lines = new string[transform.childCount];
        for (int i = 0; i < transform.childCount; i++)
        {
            lines[i] = transform.GetChild(i).position.x + " ";
            lines[i] += transform.GetChild(i).position.y + " ";
            lines[i] += transform.GetChild(i).position.z + " ";
            lines[i] += transform.GetChild(i)
                .GetComponent<BlockScript>()
                .id;
        }

        File.WriteAllLines(path, lines);
    }

    void Load()
    {
        string[] lines = File.ReadAllLines(path);
        for (int i = 0; i < lines.Length; i++)
        {
            string[] tmp = lines[i].Split(' ');

            Vector3 pos = new Vector3(
                int.Parse(tmp[0]),
                int.Parse(tmp[1]),
                int.Parse(tmp[2])
            );
            int id = int.Parse(tmp[3]);

            Instantiate(
                blocksPrefabs[id],
                pos,
                Quaternion.identity,
                this.transform
            );
        }
    }
}
