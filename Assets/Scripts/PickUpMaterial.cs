﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PickUpMaterial : MonoBehaviour
{
    public int id;
    void OnTriggerEnter(Collider other)
    {
        if (other.tag != "Player")
            return;

        other
            .gameObject
            .GetComponent<BackpackScript>()
            .ChangeMats(id - 1, 1);
            //.materialsCount[id - 1]++;
/*
        other.gameObject.GetComponent<BuildBlock>()
            .matsUI[id - 1]
            .GetComponentInChildren<Text>()
            .text = other
                .gameObject
                .GetComponent<BackpackScript>()
                .materialsCount[id - 1]
                .ToString();
*/
        Destroy(gameObject);
    }

}
