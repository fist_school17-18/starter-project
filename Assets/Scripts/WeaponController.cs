﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour {
    Camera cam;
    void Start()
    {
        cam = GetComponentInParent<Camera>();
    }
    void Update ()
    {
	    if (Input.GetButtonDown("Fire1") && transform.childCount > 0)
        {           
            Ray ray = new Ray(cam.transform.position, cam.transform.forward);
            RaycastHit hit = new RaycastHit();
            Physics.Raycast(ray, out hit, 5);
            Collider target = hit.collider;
            if (target != null)
            {
                if (target.gameObject.GetComponent<BlockScript>() != null)
                    // block
                    target.gameObject.GetComponent<BlockScript>().GetDamage(25);
                if (target.gameObject.GetComponent<Enemy>())
                    target.gameObject.GetComponent<Enemy>().GetDamage(34);
                        // enemy
            }
            GetComponentInChildren<Animator>().SetTrigger("Attack");
        }

        if (Input.GetKeyDown(KeyCode.G) &&  transform.childCount > 0)
        {
            transform.GetChild(0).GetComponent<Rigidbody>().isKinematic = false;
            transform.GetChild(0).GetComponent<PickUpWeapon>()
                .weaponSlot.gameObject.SetActive(false);
            transform.DetachChildren();
        }
    }
}
