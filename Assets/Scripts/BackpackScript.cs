﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class BackpackScript : MonoBehaviour {

    //int[] materialsCount = new int[5];
    //int[] blocksCount = new int[5];

    public RectTransform gameOverPanel;

    int[] materialsCount = { 8, 16, 32, 64, 128 };
    int[] blocksCount = { 1, 2, 4, 8, 16 };

    int HP = 40;
    [Range(1, 10)] public float respawnTime;
    float respawnTimer = 0;
    BuildBlock bb;


    void Start()
    {
        bb = GetComponent<BuildBlock>();
    }

    void Update()
    {
        if (HP <= 0)
        {
            respawnTimer += Time.deltaTime;
        }
        if (respawnTimer >= respawnTime)
        {
            gameOverPanel.gameObject.SetActive(false);
            GetComponent<FirstPersonController>().enabled = true;
            GetComponentInChildren<WeaponController>().enabled = true;
            HP = 100;
            respawnTimer = 0;
        }
    }

    public void GetDamage(int value)
    {
        HP -= value;
        print(HP);
        if (HP <= 0)
        {
            // destroy player?
            print("GAME OVER");
            gameOverPanel.gameObject.SetActive(true);
            //gameObject.SetActive(false); // 1st option
            GetComponent<FirstPersonController>().enabled = false;
            GetComponentInChildren<WeaponController>().enabled = false;
        }
    }

    public void ChangeMats(int index, int value)
    {
        materialsCount[index] += value;
        bb.matsUI[index].GetComponentInChildren<Text>().text = 
            materialsCount[index].ToString();
    }
    public void ChangeBlocks(int index, int value)
    {
        blocksCount[index] += value;
        bb.blockUI[index].GetComponentInChildren<Text>().text =
            blocksCount[index].ToString();
    }
    public int GetMatsLenght() { return materialsCount.Length; }
    public int GetBlockLenth() { return blocksCount.Length; }
    public int GetMaterialCount(int index) { return materialsCount[index]; }
    public int GetBlockCount(int index) { return blocksCount[index]; }
}