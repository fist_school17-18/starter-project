﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PickUpWeapon : MonoBehaviour {
    public Transform container;
    public Image crosshair;
    public Sprite idleCross;
    public Sprite overCross;
    public Sprite icon;
    public RectTransform weaponSlot;

    void OnMouseOver ()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            if (container.childCount > 0)
            {
                container.GetChild(0).GetComponent<Rigidbody>().isKinematic = false;
                container.DetachChildren();
            }

            transform.GetComponent<Rigidbody>().isKinematic = true;
            transform.parent = container;

            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;

            weaponSlot.gameObject.SetActive(true);
            weaponSlot.GetComponent<Image>().sprite = icon;
        }
    }

    void OnMouseEnter()
    {
        //crosshair.GetComponent<RectTransform>().sizeDelta = Vector2.one * 32;
        crosshair.GetComponent<Image>().sprite = overCross;
    }
    void OnMouseExit()
    {
        //crosshair.GetComponent<RectTransform>().sizeDelta = Vector2.one * 16;
        crosshair.GetComponent<Image>().sprite = idleCross;
    }

}
