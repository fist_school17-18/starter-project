﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CraftScript : MonoBehaviour {
    public Transform player;
    public RectTransform[] craftSlots;
    int[] blender;
    bool crafted = false;
    BackpackScript bp;

	// Use this for initialization
	void Start () {
        transform.parent.gameObject.SetActive(false);
        bp = player.GetComponent<BackpackScript>();
	}
	
    public void Craft()
    {
        crafted = false;
        blender = new int[5];
        foreach (RectTransform slot in craftSlots)
        {
            if (slot.childCount > 0)
            {
                int id = int.Parse(
                    slot
                    .GetChild(0).GetComponent<Image>()
                    .sprite
                    .name);

                blender[id]++;
            }
        }

        int blockId = System.Array.IndexOf(blender, 4);
        if (blockId >= 0)
        {
            //bp.materialsCount[blockId] -= 4;
            bp.ChangeMats(blockId, -4);
            //bp.blocksCount[blockId]++;
            bp.ChangeBlocks(blockId, 1);
            crafted = true;
        }

        if (crafted)
        {
            foreach (RectTransform slot in craftSlots)
            {
                if (slot.childCount > 0)
                    Destroy(slot.GetChild(0).gameObject);
            }
        }
    }
}
