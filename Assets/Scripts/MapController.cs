﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapController : MonoBehaviour {

    public Transform player;
    public Camera cam;

    static List<MapObject> mapObjects = new List<MapObject>(); // список объектов для радара

    public static void RegisterObject(GameObject o, Image i)
    {
        Image img = Image.Instantiate(i);
        MapObject mo = new MapObject(img, o);
        mapObjects.Add(mo);
    }

    public static void RemoveObject(GameObject owner)
    {
        MapObject mo = mapObjects.Find(s => s.owner == owner);
        Destroy(mo.icon);
        mapObjects.Remove(mo);
    }

	void Update () {
        DrawIcons();
    }

    void DrawIcons()
    {
        foreach (MapObject mo in mapObjects)
        {
            Vector2 pp = new Vector2(
                player.transform.position.x,
                player.transform.position.z
            );
            Vector2 mop = new Vector2(
                mo.owner.transform.position.x,
                mo.owner.transform.position.z
            );

            if (Vector2.Distance(mop, pp) >= 20)
            {
                mo.icon.enabled = false;
                continue;
            }
            else { mo.icon.enabled = true; }

            mo.icon.transform.SetParent(transform);
            Vector3 screenPos = 
                cam.WorldToViewportPoint(mo.owner.transform.position);

            RectTransform rt = GetComponent<RectTransform>();
            Vector3[] corners = new Vector3[4];
            rt.GetWorldCorners(corners);

            screenPos.x = Mathf.Clamp(
                screenPos.x * rt.rect.width +
                corners[0].x,
                corners[0].x,
                corners[2].x);

            screenPos.y = Mathf.Clamp(
                screenPos.y * rt.rect.height +
                corners[0].y,
                corners[0].y,
                corners[1].y
            );

            screenPos.z = 0;
            mo.icon.transform.position = screenPos;
        }
    }
}

public class MapObject
{
    public Image icon;
    public GameObject owner;

    public MapObject (Image i, GameObject o)
    {
        this.icon = i;
        this.owner = o;
    }
}
