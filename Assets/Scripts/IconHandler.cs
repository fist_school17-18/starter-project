﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IconHandler : MonoBehaviour {

    public Image icon;

	void Start () {
        // register icon on map
        MapController.RegisterObject(gameObject, icon);
	}

    void OnDestroy()
    {
        // remove icon from map
        MapController.RemoveObject(gameObject);
    }
}
